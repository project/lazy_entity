Allows field values for Drupal entities to be lazy loaded rather than loaded at
the time of the entity. This can improve performance and memory usage when
loading an entity without the need for all fields.

This is currently a developer only module. It provides both entity and controller implementations for lazy loading entities.

Note: This library does not utilize the cache_field cache when loading
the fields for an entity which can lead to a decrease in performance when you
actually want to load all fields.

Usage
=====

There are two ways to utilize the controller and entity implementations provided
by this library. You can either use `hook_entity_info_alter` to alter the
classes utilized by the entity or you could create an instance of the controller
and entity yourself.

Override entity classes
=======================

Implement `hook_entity_info_alter` in your module and replace the appropriate
classes. For example, if you wanted to lazy load node entities:

```
/**
 * Implements hook_entity_info_alter().
 *
 * Update node entities to lazy load their fields.
 */
function mymodule_entity_info_alter(&$entity_info) {
  $entity_info['node']['controller class'] = 'LazyEntityController';
  $entity_info['node']['entity class'] = 'LazyEntity';
}
```

Alternatively, if you are creating your own entity type you could set the above
indexes directly in your implementation of `hook_entity_info()`.

Direct instantiation
=====================

You can directly instantiate the controller class to load the entities. For
example:

```
// Retrieve the appropriate node ids in what ever way is appropriate for your
// implementation.
$node_ids = array(1, 2);

$controller = new LazyEntityController('node')
$nodes = $controller->load($node_ids);
```

If you already have the data for a node you can also instantiate the entity class
directly.

```
$node = new LazyEntity($node_data, 'node');
```
