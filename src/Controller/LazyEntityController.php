<?php
/**
 * @file
 * Contains LazyEntityController.
 */

/**
 * Controller for lazy loaded entities that forgoes loading the entities fields
 * until requested.
 */
class LazyEntityController extends DrupalDefaultEntityController {
  /**
   * Forgoes loading of fields for an entity in favor of lazy-loading them when
   * requested instead.
   *
   * @param array $queried_entities
   *   Associative array of query results, keyed on the entity ID.
   * @param integer $revision_id
   *   ID of the revision that was loaded, or FALSE if the most current revision
   *   was loaded.
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    // Iterate over the queried entities, converting them to lazy entities if
    // needed.
    foreach ($queried_entities as $entity_id => $entity) {
      if (!$entity instanceof LazyEntity) {
        $queried_entities[$entity_id] = new LazyEntity((array) $entity, $this->entityType);
      }
    }

    // Call hook_entity_load().
    foreach (module_implements('entity_load') as $module) {
      $function = $module . '_entity_load';
      $function($queried_entities, $this->entityType);
    }

    // Call hook_TYPE_load(). The first argument for hook_TYPE_load() are
    // always the queried entities, followed by additional arguments set in
    // $this->hookLoadArguments.
    $args = array_merge(array($queried_entities), $this->hookLoadArguments);
    foreach (module_implements($this->entityInfo['load hook']) as $module) {
      call_user_func_array($module . '_' . $this->entityInfo['load hook'], $args);
    }
  }
}
