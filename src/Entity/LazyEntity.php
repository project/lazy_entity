<?php
/**
 * @file
 * Contains LazyEntity.
 */

/**
 * Loads an entities fields on demand rather than when the entity is loaded.
 */
class LazyEntity extends Entity {
  /**
   * Field instances available for the entity based on its type and bundle.
   *
   * @var array
   */
  protected $fieldInstances;

  /**
   * Creates a new entity.
   *
   * @param array $values
   *   Array of values to be set on the entity.
   * @param string $entity_type
   *   The type of entity to be created.
   */
  public function __construct(array $values = array(), $entity_type = NULL) {
    parent::__construct($values, $entity_type);

    $this->fieldInstances = field_info_instances($this->entityType, $this->bundle());
  }

  /**
   * Retrieve a value from the entity.
   *
   * If the requested key is a property of the entity or a field that has
   * already been loaded then it will be returned immediatly. If the key is a
   * field that has not been loaded it will be loaded and the value returned.
   *
   * @throws LazyEntityInvalidPropertyException
   *
   * @param string $name
   *   The name of the property or field whose value to retrieve.
   *
   * @return mixed
   *   The value of the requested property or field.
   */
  function &__get($name) {
    // If the name is an entity property or has already been loaded then we can
    // just return it.
    if (isset($this->$name) && !is_null($this->$name)) {
      return $this->$name;
    }

    // If the name is a valid field name for this entity and bundle then load it
    // and return the value.
    if (isset($this->fieldInstances[$name])) {
      field_attach_load(
        $this->entityType,
        array($this->internalIdentifier() => $this),
        FIELD_LOAD_CURRENT,
        array(
          'field_id' => $this->fieldInstances[$name]['field_id'],
          'deleted' => TRUE,
        )
      );
      return $this->$name;
    }

    throw new LazyEntityInvalidPropertyException('Invalid property or field ' . $name . ' for entity.');
  }

  /**
   * Set the value of a property or field on the entity.
   *
   * @param string $name
   *   Name of the property or field to be set.
   * @param mixed $value
   *   Value to set for the property or field.
   */
  function __set($name, $value) {
    $this->$name = $value;
  }

  /**
   * Determines if a property or field is set on the entity.
   *
   * If the key is not currently set on the entity the field instances will be
   * checked to determine if the key is a field that can be lazy loaded.
   *
   * @param string $name
   *   Name of the property or field to check.
   *
   * @return boolean
   *   Whether or not the property or field is set on the entity.
   */
  function __isset($name) {
    return (isset($this->$name) || isset($this->fieldInstances[$name]));
  }
}
