<?php
/**
 * @file
 * Includes LazyEntityInvalidPropertyException.
 */

/**
 * Exception to be thrown when an invalid property or field is accessed on a
 * lazy loaded entity.
 */
class LazyEntityInvalidPropertyException extends OutOfBoundsException {

}
